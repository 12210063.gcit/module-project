class AppError extends Error{
    //object will take message and status code 
    constructor(message, statusCode) {
        //call parents constructor
        super(message)

        this.statusCode = statusCode
        this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error'
        //
        this.isOperational = true

        Error.captureStackTrace(this, this.constructor)
    }
}
module.exports = AppError