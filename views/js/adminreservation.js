
const fetchReservations = async () => {
  try {
    const res = await axios.get('http://localhost:4001/api/v1/reserve');
    displayReservations(res.data.data);
  } catch (err) {
    console.log(err);
  }
};
const displayReservations = (reservations) => {
  var arr = reservations;

  for (let i = 0; i < arr.length; i++) {
    var tableRow = document.createElement('tr');
    const reservation = arr[i];

    var enrollmentCell = document.createElement('td');
    enrollmentCell.textContent = reservation.enrollment;

    var dateCell = document.createElement('td');
    dateCell.textContent = reservation.date;

    var timeCell = document.createElement('td');
    timeCell.textContent = reservation.time;

    var gameTypeCell = document.createElement('td');
    gameTypeCell.textContent = reservation.game_type;

    var deleteCell = document.createElement('td');
    deleteCell.classList.add('delete-btn');
    deleteCell.textContent = 'Delete';
    deleteCell.addEventListener('click', async () => {
      try {
        await axios.delete(`http://localhost:4001/api/v1/reserve/${reservation._id}`);
        tableRow.remove();
      } catch (err) {
        console.log(err);
      }
    });

    tableRow.appendChild(enrollmentCell);
    tableRow.appendChild(dateCell);
    tableRow.appendChild(timeCell);
    tableRow.appendChild(gameTypeCell);
    tableRow.appendChild(deleteCell);

    var table = document.querySelector('.table');
    table.querySelector('tbody').appendChild(tableRow);
  }
};

fetchReservations();