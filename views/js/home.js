const fetchNews = async () => {
    try {
      const res = await axios({
        method: 'GET',
        url: 'http://localhost:4001/api/v1/news/displayallnews',
      });
     
      displayNews(res.data.data)
    } catch (error) {
      console.log(error);
    }
  };

  fetchNews();

  const displayNews = (news) => {
    var arr = news
    console.log(arr)
    for (let i=0; i<arr.length; i++) {
        const element = arr[i]
        var card = document.querySelector('#newsContainer').cloneNode(true)
        card = addItemContent(card, element)


        var card2 = document.querySelector('#newsContainer')
        card2.insertAdjacentElement('afterend', card)
    }
    document.querySelector('#newsContainer').remove()
}


function addItemContent(card, element){
    
        var el1 = card.querySelector('.card__picture-img')
        var el2 = card.querySelector('.card__sub-heading')
        var el3 = card.querySelector('.card__text')
        var el4 = card.querySelector('.card__date')
       
        el1.src = '/img/news/'+element.imageCover
        el2.innerHTML = ''+element.title
        el3.innerHTML = ' '+element.content
        el4.innerHTML =''+element.publishedAt

        return card
}