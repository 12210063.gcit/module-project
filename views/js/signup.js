import {showAlert} from "./alert.js"

export const signup = async(username,enrollment,email,password,passwordConfirm)=>{
    try{
        const res = await axios({
            method:'POST',
            url:'http://localhost:4001/api/v1/users/signup',
            data:{
                username,
                email,
                enrollment,
                password,
                passwordConfirm,
                
            },
        })
        if(res.data.status==='success'){
            showAlert('success','Account created successfully')
            window.setTimeout(()=>{
                location.assign('/login')
            },1500)
        }

    }catch(err){
        let message = typeof err.response !== 'undefined'? 
        err.response.data.message
        :err.message
        showAlert('error','Error: Password are not the same!',message)
    }
}
document.querySelector('.form').addEventListener('submit',(e)=>{
    e.preventDefault()
    e.preventDefault()
    const username = document.getElementById('username').value
    console.log('username',username)
    const enrollment = document.getElementById('enrollment').value
    const email = document.getElementById('email').value
    const password = document.getElementById('password').value
    const passwordConfirm = document.getElementById('passwordConfirm').value
    signup(username,enrollment,email,password,passwordConfirm)

})