const fetchUser = async () =>{
   try {
    const res = await axios({
        method :'GET',
        url:'http://localhost:4001/api/v1/users'
    })
    displayAllUser(res.data.data)

    
   } catch (error) {
    console.log(error)
    
   }
    

}

const displayAllUser = (user) =>{
    var arr = user
    for (let i = 0; i < arr.length; i++) {
        var tableRow = document.createElement('tr');
        const users = arr[i];
    
        var enrollmentCell = document.createElement('td');
        enrollmentCell.textContent = users.enrollment;
    
        var dateCell = document.createElement('td');
        dateCell.textContent = users.username;
    
        var timeCell = document.createElement('td');
        timeCell.textContent = users.email;

        tableRow.appendChild(enrollmentCell);
        tableRow.appendChild(dateCell);
        tableRow.appendChild(timeCell);
     
    
    
        var table = document.querySelector('.table');
        table.querySelector('tbody').appendChild(tableRow);
      }
}
fetchUser()