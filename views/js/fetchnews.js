const fetchNews = async () => {
    try {
        const res = await axios({
            method: 'GET',
            url: 'http://localhost:4001/api/v1/news/displayallnews',
        });
        displayAllNews(res.data);
    } catch (err) {
        console.log(err);
    }
};

const displayAllNews = (news) => {
    var arr = news.data;
    var container = document.querySelector('.row');
    var template = document.querySelector('.news');

    for (let i = 0; i < arr.length; i++) {
        var newsItem = arr[i];

        var clone = template.cloneNode(true);
        clone.querySelector('.headline').textContent = newsItem.title;
        clone.querySelector('.imageCover').src = newsItem.imageCover;
        clone.querySelector('.Published_at').textContent = newsItem.publishedAt;
        clone.querySelector('.content').textContent = newsItem.content;

        container.appendChild(clone);
    }
};

fetchNews();
