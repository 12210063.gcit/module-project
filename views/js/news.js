// import { showAlert } from "./alert.js"

// export const news = async(title,imageCover,content)=>{
//     try {
//         const res = await axios({
//             method:'POST',
//             url:'http://localhost:4001/api/v1/news/addnews',
//             data:{
//                 title,
//                 imageCover,
//                 content,
//             },
//         })
//         if (res.data.status === 'success') {
//             showAlert('success', 'News Added')
//             // window.setTimeout(() => {
//             //     location.assign('/reservationpage')
//             // }, 1500)
//         }
//     } catch (error) {
//         console.log(error)
        
//     }
// }
// document.querySelector('form').addEventListener('submit',(e)=>{
//     e.preventDefault()
//     const title = document.getElementById('title').value
//     const imageCover = document.getElementById('imageCover').value
//     const content = document.getElementById('content').value
//     news(title,imageCover,content)
// })


import { showAlert } from "./alert.js"

export const news = async (title, imageCover, content) => {
  try {
    const formData = new FormData();
    formData.append('title', title);
    formData.append('imageCover', imageCover); // Append the file to the FormData object
    formData.append('content', content);

    const res = await axios({
      method: 'POST',
      url: 'http://localhost:4001/api/v1/news/addnews',
      data: formData, // Send the FormData object as the request data
      headers: { 'Content-Type': 'multipart/form-data' }, // Set the appropriate headers
    });

    if (res.data.status === 'success') {
      showAlert('success', 'News Added');
      // window.setTimeout(() => {
      //     location.assign('/reservationpage')
      // }, 1500)
    }
  } catch (error) {
    console.log(error);
  }
}

document.querySelector('form').addEventListener('submit', (e) => {
  e.preventDefault();
  const title = document.getElementById('title').value;
  const imageCover = document.getElementById('imageCover').files[0]; // Get the file from the file input element
  // const content11 = document.getElementById('content').value;
  const content111 = document.getElementById('content111').value;

  console.log(content111)
  news(title, imageCover, content111);
});
