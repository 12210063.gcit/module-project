const fetchNews = async () => {
  try {
    const res = await axios({
      method: 'GET',
      url: 'http://localhost:4001/api/v1/news/displayallnews',
    });
   
    displayNews(res.data.data)
  } catch (error) {
    console.log(error);
  }
};

fetchNews();

const displayNews = (news) => {
  var arr = news
  console.log(arr)
  for (let i=0; i<arr.length; i++) {
      const element = arr[i]
      var card = document.querySelector('#newsContainer').cloneNode(true)
      card = addItemContent(card, element)


      var card2 = document.querySelector('#newsContainer')
      card2.insertAdjacentElement('afterend', card)
  }
  document.querySelector('#newsContainer').remove()
}


function addItemContent(card, element){
  
      var el1 = card.querySelector('.card__picture-img')
      var el2 = card.querySelector('.card__sub-heading')
      var el3 = card.querySelector('.card__text')
      var el4 = card.querySelector('.card__date')
      var deleteButton = card.querySelector('.delete-button');

     
      el1.src = '/img/news/'+element.imageCover
      el2.innerHTML = ''+element.title
      el3.innerHTML = ' '+element.content
      el4.innerHTML =''+element.publishedAt
      
      deleteButton.addEventListener('click', () => {
        deleteNewsItem(element._id);
      });
      return card
}

function deleteNewsItem(itemId) {
  // Make a request to delete the news item with the given ID
  // For example:
  axios({
    method: 'DELETE',
    url: `http://localhost:4001/api/v1/news/displayallnews/${itemId}`,
  })
    .then((response) => {
      // Handle success
      console.log('News item deleted:', response.data);
      // Remove the deleted item from the DOM
      var itemToRemove = document.getElementById(`news-item-${itemId}`);
      if (itemToRemove) {
        itemToRemove.remove();
      }
       location.reload();
    })
    .catch((error) => {
      // Handle error
      console.log('Error deleting news item:', error);
    });
}
