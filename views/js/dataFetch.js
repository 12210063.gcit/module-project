import { showAlert } from "./alert.js"

export const fetchReservations = async () => {
  try {
    const res = await axios.get('http://localhost:4001/api/v1/reserve');
    if (res.data.status === 'success') {
      return res.data.data;
    }
  } catch (err) {
    let message = typeof err.response !== 'undefined' ?
      err.response.data.message :
      err.message;
    showAlert('error', 'Unable to fetch reservations', message);
  }
};

export const reservation = async(enrollment, date, time, game_type) => {
  try {
      const res = await axios({
          method: 'POST',
          url: 'http://localhost:4001/api/v1/reserve/book',
          data: {
              enrollment,
              date,
              time,
              game_type,

          },
      })
      if (res.data.status === 'success') {
          showAlert('success', 'You reserved the ground')
          window.setTimeout(() => {
              location.assign('/reservationpage')
          }, 1500)
      }

  } catch (err) {
      let message = typeof err.response !== 'undefined' ?
          err.response.data.message :
          err.message
      showAlert('error', 'unable to book the ground', message)
  }
}
