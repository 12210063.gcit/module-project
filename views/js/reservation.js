const fetchReservations = async () => {
  try {
    const res = await axios({
      method: 'GET',
      url: 'http://localhost:4001/api/v1/reserve',
    });
    displayReservations(res.data.data);
    
  } catch (err) {
    console.log(err);
  }
};

const displayReservations = (reservations) => {
  var arr = reservations;

  for (let i = 0; i < arr.length; i++) {
    var tableRow = document.createElement('tr');
    const reservation = arr[i];

    var enrollmentCell = document.createElement('td');
    enrollmentCell.textContent = reservation.enrollment;

    var dateCell = document.createElement('td');
    dateCell.textContent = reservation.date;

    var timeCell = document.createElement('td');
    timeCell.textContent = reservation.time;

    var gameTypeCell = document.createElement('td');
    gameTypeCell.textContent = reservation.game_type;

  


    tableRow.appendChild(enrollmentCell);
    tableRow.appendChild(dateCell);
    tableRow.appendChild(timeCell);
    tableRow.appendChild(gameTypeCell);


    var table = document.querySelector('.table');
    table.querySelector('tbody').appendChild(tableRow);
  }
};


const handleEdit = async (reservationId) => {
  // Get the updated values from the user
  const enrollmentInput = prompt('Enter updated enrollment:');
  const dateInput = prompt('Enter updated date:');
  const timeInput = prompt('Enter updated time:');
  const gameTypeInput = prompt('Enter updated game type:');

  // Create an object with the updated data
  const updatedData = {
    enrollment: enrollmentInput,
    date: dateInput,
    time: timeInput,
    game_type: gameTypeInput
  };

  try {
    // Make a PUT or PATCH request to update the reservation
    const res = await axios.put(`http://localhost:4001/api/v1/reserve/${reservationId}`, updatedData);

    // Handle the response as needed
    console.log('Reservation updated:', res.data);

    // Update the corresponding table row with the new values
    const table = document.querySelector('.table');
    const rows = table.querySelectorAll('tbody tr');
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];
      const rowReservationId = row.getAttribute('data-id');
      if (rowReservationId === reservationId) {
        const cells = row.querySelectorAll('td');
        cells[0].textContent = enrollmentInput;
        cells[1].textContent = dateInput;
        cells[2].textContent = timeInput;
        cells[3].textContent = gameTypeInput;
        break;
      }
    }
  } catch (err) {
    console.log(err);
  }
};



const handleDelete = async (reservationId, tableRow) => {
  // Add code to handle delete functionality
  try {
    await axios.delete(`http://localhost:4001/api/v1/reserve/${reservationId}`);
    tableRow.remove();
  } catch (err) {
    console.log(err);
  }
};

fetchReservations();