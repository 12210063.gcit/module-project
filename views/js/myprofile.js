var obj = JSON.parse(document.cookie.substring((6)))


var el = document.querySelector('.card .user_details')

if(obj._id){
  
  el.innerHTML = '<h3 class="username">'+obj.username+'</h3>'

}
var el1 = document.querySelector('.card .card_profile_img')

if(obj._id){
  el1.innerHTML =' <img src="../img/users/' +
  obj.photo +
  '"alt="Photo of ${user.username}" class="nav__user-img" />'
}


var el4 = document.querySelector('.form .user-data')
el4.innerHTML =
  ` <div class="form__group"> <label class="form__label" for="username">UserName</label> <input class="form__input" id="username" type="text" value="` +
  obj.username.toUpperCase() +
  `" required="required" name="username"/></div><div class="form__group ma-bt-md"><label class="form__label" for="email">Email address</label>
<input class="form__input" id="email" type="email" value="` +
  obj.email +
  `" required="required" name="email"/>
</div>
<div class="form__group form__photo-upload"><img class="form__user-photo" src="../img/users/` +
  obj.photo +
  `" alt="User photo"/><input class="form__upload" type="file" accept="image/*" id="photo" name="photo"/><label for="photo">Choose new photo</label></div>


<div class "form__group right">  <button class="btn btn--small btn--green">Save settings</button></div>`


var el5 = document.querySelector('.form .user-password')
el5.innerHTML = `   <div class="form__group">
                        <label class="form__label" for="password-current">Current password</label>
                        <input class="form__input" id="password-current" type="password" placeholder="••••••••"
                            required="required" minlength="8" />
                    </div>
                    <div class="form__group">
                        <label class="form__label" for="password">New password</label>
                        <input class="form__input" id="password" type="password" placeholder="••••••••"
                            required="required" minlength="8" />
                    </div>
                    <div class="form__group ma-bt-lg">
                        <label class="form__label" for="passwordConfirm">Confirm password</label>
                        <input class="form__input" id="passwordConfirm" type="password" placeholder="••••••••"
                            required="required" minlength="8" />
                    </div>
                    <div class="form__group right">
                            <button class="btn btn--small btn--green btn--save-password">Save password</button>
                    </div>`


 
// type is either 'password' or data


const fetchReservations = async () => {
  try {
    const res = await axios({
      method: 'GET',
      url: 'http://localhost:4001/api/v1/reserve',
    });
    displayReservations(res.data.data);
    
  } catch (err) {
    console.log(err);
  }
};

const displayReservations = (reservations) => {
  const arr = reservations;
  console.log(arr);
  if (obj._id) {
    for (let i = 0; i < arr.length; i++) {
      const reservation = arr[i];
      if (obj.enrollment === reservation.enrollment) {
        const tableRow = createTableRow(reservation);
        appendTableRow(tableRow);
      }
    }
  }
};

const createTableRow = (reservation) => {
  const tableRow = document.createElement('tr');

  const enrollmentCell = document.createElement('td');
  enrollmentCell.textContent = reservation.enrollment;

  const dateCell = document.createElement('td');
  dateCell.textContent = reservation.date;

  const timeCell = document.createElement('td');
  timeCell.textContent = reservation.time;

  const gameTypeCell = document.createElement('td');
  gameTypeCell.textContent = reservation.game_type;

  const actionsCell = document.createElement('td');

  const editButton = document.createElement('button');
  editButton.classList.add('edit', 'btn');
  editButton.textContent = 'Edit';
  editButton.addEventListener('click', () => {
    redirectToUpdateForm(reservation._id);
  });

  const deleteButton = document.createElement('button');
  deleteButton.classList.add('delete', 'btn');
  deleteButton.textContent = 'Delete';
  deleteButton.addEventListener('click', () => {
    handleDelete(reservation._id, tableRow);
  });

  actionsCell.appendChild(editButton);
  actionsCell.appendChild(deleteButton);

  tableRow.appendChild(enrollmentCell);
  tableRow.appendChild(dateCell);
  tableRow.appendChild(timeCell);
  tableRow.appendChild(gameTypeCell);
  tableRow.appendChild(actionsCell);

  return tableRow;
};

const appendTableRow = (tableRow) => {
  const table = document.querySelector('.table');
  table.querySelector('tbody').appendChild(tableRow);
};

const redirectToUpdateForm = (reservationId) => {
  window.location.href = `updateform.html?id=${reservationId}`;
};

fetchReservations()

const logout = async ()=>{
  try {
    const res = await axios({
      method:'GET',
      url:'http://localhost:4001/api/v1/users/logout'
    })
    if(res.data.status == 'success'){
      window.setTimeout(()=> {
        location.assign('/')
    })
    }
    
  } catch (error) {
    console.log(error)
    
  }
}

var doc = document.querySelector('#logout')

doc.addEventListener('click', (e) => logout())
