const FootballTeam = require('../models/teamModel');

// Create a new football team
const createTeam = async (req, res) => {
  try {
    const { name, coach, stadium, players } = req.body;
    const team = await FootballTeam.create({ name, coach, stadium, players });
    res.status(201).json({ team });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Get all football teams
const getAllTeams = async (req, res) => {
  try {
    const teams = await FootballTeam.find();
    res.status(200).json({ teams });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

module.exports = {
  createTeam,
  getAllTeams,
};
