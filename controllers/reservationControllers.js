const reservation = require('../models/reservationModel');

exports.createReservation = async (req,res)=>{
    try {
        const Reservation = await reservation.create(req.body)
        res.status(201).json({
            data: {
                Reservation:Reservation,
            },
             status: 'success'});
    } catch (err) {
        res.status(400).json({error: err.message});
        
    }
}

exports.getAllReservation = async (req,res)=>{
    try {
        const Reservation = await reservation.find()
        res.status(200).json({
            data: Reservation, 
            status: 'success',
            results: Reservation.length,
        })
       
    } catch (err) {
        res.status(404).json({
            status: 'fail',
            message: 'Invalid data sent!',
          })
        
    }
}
exports.getReservation = async (req,res)=>{
    try {
        const Reservation = await reservation.findById(req.param.id);
        res.json({data: Reservation,status:"success"});
    } catch (err) {
        res.status(500).json({error: err.message});
        
    }
}

exports.deleteReservation = async (req, res) =>{
    try {
        const Reservation = await reservation.findByIdAndDelete(req.params.id);
        res.json({ data: Reservation, status: 'success' });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
  }

  exports.updateReservation = async (req, res) => {
    try {
        const Reservation = await reservation.findByIdAndUpdate(req.params.id, req.body);
        res.json({data: Reservation, status: "success"});
    }catch (err) {
        res.status(500).json({error: err.message});
    }
}

