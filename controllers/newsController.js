const News = require('../models/newsModel');
const multer = require('multer')

const multerStorage = multer.diskStorage({
    
  destination:(req, file, cb) => {
      cb(null, 'views/img/news')
  },
  filename: async(req, file, cb)=>{

      //user-id-currentimestamp.extension
      const ext = file.mimetype.split('/')[1]
      cb(null, `user-${"decoded.id"}-${Date.now()}.${ext}`)
  },
})
const multerFilter = (req, file, cb) => {
  if(file.mimetype.startsWith('image')){
      cb(null, true)
  }
  else{
      cb(new AppError('Not an image! Please upload only images', 400), false)
  }
}

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter
})

exports.uploadItemPhoto = upload.single('imageCover')

exports.createNews = async (req, res) => {
  try{
      // console.log(req.headers)
      if(req.file){
        req.body.imageCover = req.file.filename
      }
      const news = await News.create(req.body);
      res.json({data: news, status: "success"});
  }catch (err){
      res.status(500).json({error: err.message});
  }
}

exports.getAllNews = async (req, res) => {
  try {
    const news = await News.find();
    res.status(200).json({
      data: news,
      status: 'success',
      result: news.length,
    });
  } catch (error) {
    res.status(404).json({
      status: 'fail',
      message: 'Invalid data sent',
    });
  }
};

exports.deleteNews = async (req, res) => {
  try {
    const news = await News.findByIdAndDelete(req.params.id);
    if (!news) {
      return res.status(404).json({ message: 'News not found' });
    }
    res.status(200).json({ message: 'News deleted successfully' });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};