const path = require('path')

/* Landing PAGE */

exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'landing.html'))
}

exports.getHomePage = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'home.html'))
}
exports.getlogin = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'login.html'))
}
exports.getsignup = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'signup.html'))
}
exports.getprofile = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'myprofilepage.html'))
}

exports.getprofile = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'myprofilepage.html'))
}

exports.getReservation = (req,res)=>{
    res.sendFile(path.join(__dirname, '../', 'views', 'reservation.html'))    
}
exports.getBooked = (req,res)=>{
    res.sendFile(path.join(__dirname, '../', 'views', 'booking.html'))    
}

exports.getTeam = (req,res)=>{
    res.sendFile(path.join(__dirname, '../', 'views', 'team.html'))    
}

exports.getDashboard = (req,res)=>{
    res.sendFile(path.join(__dirname, '../', 'views', 'dashboard.html'))    
}

exports.getnews = (req,res)=>{
    res.sendFile(path.join(__dirname, '../', 'views', 'news.html'))    
}

exports.getTournament = (req,res)=>{
    res.sendFile(path.join(__dirname, '../', 'views', 'football.html'))    
}
exports.getAdminReservation = (req,res)=>{
    res.sendFile(path.join(__dirname, '../', 'views', 'adminReservation.html'))    
}

exports.getUpdateForm = (req,res)=>{
    res.sendFile(path.join(__dirname, '../', 'views', 'updateform.html'))    
}
exports.getlaodnews = (req,res)=>{
    res.sendFile(path.join(__dirname, '../', 'views', 'loadnews.html'))    
}