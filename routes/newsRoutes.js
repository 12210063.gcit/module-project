const express = require('express');
const newsController = require('./../controllers/newsController');
const router = express.Router()



router.post('/addnews',newsController.uploadItemPhoto,newsController.createNews)

router.get('/displayallnews',newsController.getAllNews)

router.delete('/displayallnews/:id',newsController.deleteNews)


module.exports = router;
