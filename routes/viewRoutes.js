const express = require("express");
const router = express.Router()
const viewController = require('../controllers/viewController')

router.get('/',viewController.getHome)

router.get('/home',viewController.getHomePage)

router.get('/login',viewController.getlogin)

router.get('/signup',viewController.getsignup)

router.get('/me',viewController.getprofile)

router.get('/reservationpage',viewController.getReservation)

router.get('/book',viewController.getBooked)

router.get('/team',viewController.getTeam)

router.get('/dashboard',viewController.getDashboard)

router.get('/news',viewController.getnews)

router.get('/tournament',viewController.getTournament)

router.get('/adminReservation',viewController.getAdminReservation)

router.get('/updateform',viewController.getUpdateForm)

router.get('/loadnews',viewController.getlaodnews)

module.exports = router