const express = require('express');
const reservation = require('./../controllers/reservationControllers')
const router = express.Router()

router.post('/book',reservation.createReservation)
router.get('/reservationpage',reservation.getAllReservation)
router.delete('/:id',reservation.deleteReservation)
router.put('/:id',reservation.updateReservation)



router
    .route('/')
    .get(reservation.getAllReservation)
    .post(reservation.createReservation)
    .delete(reservation.deleteReservation)
   
 module.exports = router