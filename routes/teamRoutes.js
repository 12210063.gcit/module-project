const express = require('express');
const teamController = require('../controllers/teamController');

const router = express.Router();

// Create a new football team
router.post('/addteam', teamController.createTeam);

// Get all football teams
router.get('/', teamController.getAllTeams);

module.exports = router;
