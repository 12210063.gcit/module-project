const express = require("express")

const path = require('path')
const app = express()
const bodyParser = require('body-parser');
// const cookieParser = require('cookie-parser');

const userRouter = require('./routes/userRoutes')

const viewRoutes = require('./routes/viewRoutes')

const teamRoutes = require('./routes/teamRoutes')

const reservationRoutes = require('./routes/reservationRoutes')

const newsRoutes = require('./routes/newsRoutes')



app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(cookieParser)
app.use('/api/v1/users', userRouter)

app.use('/api/v1/reserve', reservationRoutes)

app.use('/api/v1/teams', teamRoutes)

app.use('/api/v1/news',newsRoutes)



app.use('/', viewRoutes)
app.use(express.static(path.join(__dirname, 'views')))



module.exports = app