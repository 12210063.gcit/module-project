const mongoose = require('mongoose');

const reservationSchema = new mongoose.Schema({
  enrollment: {
    type: String,
    required: [true,'Please provide your enrollment'],
  },
  date: {
    type: String,
    required: [true, 'Enter Date.......'],
    validate: {
      validator: async function (value) {
        const count = await this.model('Reservation').countDocuments({
          date: value,
          time: this.parent().time,
          game_type: this.parent().game_type,
        });
        return count === 0; // Returns true if no matching documents are found
      },
      message: 'Ground is already booked at the specified date and time',
    },
  },
  time: {
    type: String,
    required: [true, 'Enter time......'],
    validate: {
      validator: async function (value) {
        const count = await this.model('Reservation').countDocuments({
          date: this.parent().date,
          time: value,
          game_type: this.parent().game_type,
        });
        return count === 0; // Returns true if no matching documents are found
      },
      message: 'Ground is already booked at the specified date and time',
    },
  },
  game_type: {
    type: String,
    default: 'Football',
    validate: {
      validator: async function (value) {
        const count = await this.model('Reservation').countDocuments({
          date: this.parent().date,
          time: this.parent().time,
          game_type: value,
        });
        return count === 0; // Returns true if no matching documents are found
      },
      message: 'Ground is already booked with the specified game type at the specified date and time',
    },
  },
});

const Reservation = mongoose.model('Reservation', reservationSchema);

module.exports = Reservation;
