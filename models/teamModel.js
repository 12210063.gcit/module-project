const mongoose = require('mongoose');

const footballTeamSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  coach: {
    type: String,
    required: true,
  },
  stadium: {
    type: String,
    required: true,
  },
  players: [
    {
      name: {
        type: String,
        required: true,
      },
      position: {
        type: String,
        required: true,
      },
    },
  ],
});

const FootballTeam = mongoose.model('FootballTeam', footballTeamSchema);

module.exports = FootballTeam;
