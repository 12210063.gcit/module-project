const mongoose = require('mongoose')

const newsFeedsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'A name should be unique'],
    },
    publishedAt: {
        type: Date,
        default: Date.now(),
    },
    imageCover: {
        type: String,
        default:"default.jpg",
    }, 
    content: {
        type: String,
        required: true,
        trim: true,
    },
     
})
const newsFeed = mongoose.model('NewsFeeds', newsFeedsSchema)
module.exports = newsFeed